# MVC Template

Template project for future work which uses the following:  
  - MVC3  
  - Razor  
  - jQuery, jQuery-UI   
  - IOC/DI with Unity  
  - AutoMapper - model mapping   
  - Entity Framework  
  - Repository pattern, implemented with Generic Repositories <T>   
  - Unit Of Work - single atomic management of work.  