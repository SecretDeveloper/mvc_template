﻿using WeddingMVC.Initializers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using Microsoft.Practices.Unity;

using Unity;
using Unity.AutoRegistration;

using WeddingMVC;
using WeddingMVC.DAL;
using WeddingMVC.DAL.Interfaces;
using System.Web.Mvc;


namespace WeddingMVC.Tests
{   
    /// <summary>
    ///This is a test class for UnityInitializerTest and is intended
    ///to contain all UnityInitializerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class UnityInitializerTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///A test for UnityInitializer Constructor
        ///</summary>
        [TestMethod()]
        public void UnityInitializerGenericRepositoryTest()
        {
            UnityInitializer target = new UnityInitializer();
            target.Initialize();
            //var container = UnityInitializer.BuildUnityContainer();

            //Assert.IsTrue(container.IsRegistered(typeof(DAL.Interfaces.IRepository<DAL.Models.Invitee>)));

            var I = DependencyResolver.Current.GetService<IRepository<WeddingMVC.DAL.Models.Invitee>>();
            var P = DependencyResolver.Current.GetService<IRepository<string>>();
            Assert.IsNotNull(I);            
            Assert.IsNotNull(P);
        }

    }
}
