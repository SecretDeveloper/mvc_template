﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace WeddingMVC.Models
{
    public class Invitee
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [StringLength(4, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
        [DataType(DataType.Text)]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Number attending")]        
        public int NumberAttending { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Max Invited")]
        public int MaxInvited { get; set; }
    }
}