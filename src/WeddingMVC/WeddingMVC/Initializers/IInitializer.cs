﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeddingMVC.Initializers
{
    public interface IInitializer
    {
        void Initialize();
    }
}