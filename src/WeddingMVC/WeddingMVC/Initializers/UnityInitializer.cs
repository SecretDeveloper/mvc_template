﻿using System.Web.Mvc;
using System.Collections.Generic;

using Microsoft.Practices.Unity;
using Unity;
using Unity.Mvc3;
using Unity.AutoRegistration;
using System.Data.Entity;

using WeddingMVC.DAL.Interfaces;
using WeddingMVC.DAL;

namespace WeddingMVC.Initializers
{
    public class UnityInitializer:IInitializer
    {
        public void Initialize()
        {
            var container = BuildUnityContainer();            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        public static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();
            
            // Register GenericRepository<> types.
            /*var allowedActions = new List<System.Type> { typeof(GenericRepository<>) };
            container.ConfigureAutoRegistration()
                .LoadAssembliesFrom(new List<string>(){"WeddingMVC.DAL.dll"})
                .Include(type => type.ImplementsOpenGeneric(typeof(IRepository<>)),
                    Then.Register().AsFirstInterfaceOfType().WithTypeName())
                .ApplyAutoRegistration();                
            */

            // register all your components with the container here
            // e.g. container.RegisterType<ITestService, TestService>();            
            container.RegisterType<System.Data.Entity.DbContext, WeddingContext>("DbContext");
            container.RegisterType<WeddingContext>("WeddingContext");            
            
            // Register the generic repository
            //container.RegisterType(typeof(IRepository<string>), typeof(GenericRepository<string>), "Repository - Test - Remove");
            container.RegisterType(typeof(IRepository<>), typeof(GenericRepository<>), "Repository");
            container.RegisterType(typeof(IRepository<WeddingMVC.DAL.Models.Invitee>), typeof(GenericRepository<WeddingMVC.DAL.Models.Invitee>), "Repository - Invitee");

            container.RegisterType<DAL.UnitOfWork>(
                new InjectionConstructor(
                    new object[] 
                    { container.Resolve<WeddingContext>()
                    , container.Resolve<GenericRepository<WeddingMVC.DAL.Models.Invitee>>() 
                    }));
            
            container.RegisterControllers();            

            return container;
        }
    }
}