﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeddingMVC.Initializers
{
    public class AutoMapperInitializer:IInitializer
    {
        void IInitializer.Initialize()
        {
            AutoMapper.Mapper.Initialize(x =>
            {
                // Add the ViewModel - > Dal.Model mappings.
                x.AddProfile<ViewModelToDataModelAutoMapperProfile>();
            });
        }
    }
}