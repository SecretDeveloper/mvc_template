﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace WeddingMVC.Initializers
{
    /// <summary>
    /// Initialises the Entity Framework when the Application Starts.
    /// </summary>
    public class EntityFrameworkInitializer:IInitializer
    {
        public void Initialize()
        {
            bool dropAndRecreate = false;
            string val = System.Configuration.ConfigurationManager.AppSettings["DropAndRecreateSchema"];

            if (!string.IsNullOrEmpty(val))
                if (val.Trim().Equals("true", StringComparison.InvariantCultureIgnoreCase))
                    dropAndRecreate = true;

            if(dropAndRecreate)
                Database.SetInitializer<WeddingMVC.DAL.WeddingContext>(new DAL.Initializers.WeddingDBDropCreateAlwaysInitializer());
            else
                Database.SetInitializer<WeddingMVC.DAL.WeddingContext>(new DAL.Initializers.WeddingDBCreateDatabaseIfNotExistsInitializer());
        }
    }
}