using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc3;
using System.Collections.Generic;
using WeddingMVC.Initializers;
using System;
using System.Linq;

namespace WeddingMVC
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {

            // Find all IInitializer items and initialize them.
            var iInitializer = typeof(IInitializer);

            List<IInitializer> initializers = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => iInitializer.IsAssignableFrom(p) && p.IsClass)
                .Select(x => (IInitializer)Activator.CreateInstance(x)).ToList();

            foreach (var initializer in initializers)
            {                
                initializer.Initialize();
            }
        }

        
    }
}