﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WeddingMVC.Controllers
{
    public class InviteeController : Controller
    {
        private DAL.UnitOfWork _unitOfWork = DependencyResolver.Current.GetService<DAL.UnitOfWork>();
        
        //
        // GET: /Invitee/

        public ActionResult Index()
        {
            var list = new List<WeddingMVC.Models.Invitee>();

            var invitees = _unitOfWork.InviteeRepository.GetAll().ToList<WeddingMVC.DAL.Models.Invitee>();

            foreach (var inv in invitees)
            {
                var t = AutoMapper.Mapper.Map<WeddingMVC.DAL.Models.Invitee, WeddingMVC.Models.Invitee>(inv);
                list.Add(t);
            }

            return View(list);
        }

        //
        // GET: /Invitee/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Invitee/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Invitee/Create

        [HttpPost]
        public ActionResult Create(Models.Invitee invitee)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var entity = AutoMapper.Mapper.Map<WeddingMVC.Models.Invitee, WeddingMVC.DAL.Models.Invitee>(invitee);
                    _unitOfWork.InviteeRepository.Add(entity);
                    _unitOfWork.Save();
                }
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                //TOdo : Log the error somewhere.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
            }

            return View(invitee);
        }
        
        //
        // GET: /Invitee/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Invitee/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Invitee/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Invitee/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
