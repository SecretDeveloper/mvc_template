﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeddingMVC
{
    public class ViewModelToDataModelAutoMapperProfile: AutoMapper.Profile
    {
        public override string ProfileName
        {
            get
            {
                return "ViewModelToDataModel";
            }
        }

        protected override void Configure()
        {
            base.Configure();

            CreateMap<WeddingMVC.Models.Invitee, WeddingMVC.DAL.Models.Invitee>();
            CreateMap<WeddingMVC.DAL.Models.Invitee, WeddingMVC.Models.Invitee>();
        
        }
    }
}