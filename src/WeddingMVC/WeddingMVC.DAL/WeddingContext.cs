﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace WeddingMVC.DAL
{
    public class WeddingContext : DbContext
    {
        public WeddingContext():base()
        {

        }

        public DbSet<Models.Invitee> Invitees { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Entity relationships defined here.
        }
    }
}
