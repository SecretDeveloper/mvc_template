﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Entity;

namespace WeddingMVC.DAL.Initializers
{
    /// <summary>
    /// Will only create the schema if it does not already exist.
    /// </summary>
    public class WeddingDBCreateDatabaseIfNotExistsInitializer : CreateDatabaseIfNotExists<WeddingContext>
    {
        protected override void Seed(WeddingContext context)
        {
            //
        }
    }
}
