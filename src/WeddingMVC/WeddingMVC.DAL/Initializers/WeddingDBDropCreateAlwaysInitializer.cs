﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Entity;

namespace WeddingMVC.DAL.Initializers
{
    /// <summary>
    /// Will drop and recreate the Schema everytime.
    /// </summary>
    public class WeddingDBDropCreateAlwaysInitializer:DropCreateDatabaseAlways<WeddingContext>
    {
        protected override void Seed(WeddingContext context)
        {
            var invitees = new List<DAL.Models.Invitee>
            {
                new Models.Invitee { Code="AAAA", MaxInvited=1, Name="A AAA", NumberAttending =0},
                new Models.Invitee { Code="BBBB", MaxInvited=2, Name="B BBB", NumberAttending =0},
                new Models.Invitee { Code="CCCC", MaxInvited=3, Name="C CCC", NumberAttending =0},
                new Models.Invitee { Code="DDDD", MaxInvited=4, Name="D DDD", NumberAttending =0},
                new Models.Invitee { Code="EEEE", MaxInvited=5, Name="E EEE", NumberAttending =0},
                new Models.Invitee { Code="FFFF", MaxInvited=6, Name="F FFF", NumberAttending =0},
            };

            invitees.ForEach(i => context.Invitees.Add(i));

            context.SaveChanges();
        }
    }
}
