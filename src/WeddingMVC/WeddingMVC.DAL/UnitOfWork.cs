﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Entity;

namespace WeddingMVC.DAL
{
    public class UnitOfWork : IDisposable
    {
        private WeddingContext mContext;
        private Interfaces.IRepository<Models.Invitee> mInviteeRepository;

        public UnitOfWork(WeddingContext weddingContext, Interfaces.IRepository<Models.Invitee> inviteeRepository)
        {
            mContext = weddingContext;
            mInviteeRepository = inviteeRepository;

            // repo should have null context
            if(mInviteeRepository.GetContext() == null)
                mInviteeRepository.SetContext(mContext);
        }

        public Interfaces.IRepository<Models.Invitee> InviteeRepository
        {
            get
            {                
                return this.mInviteeRepository;
            }
        }

        public void Save()
        {
            mContext.SaveChanges();
        }


        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    mContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
