﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace WeddingMVC.DAL.Models
{
    public class Invitee
    {
        public string Name { get; set; }
        public int ID { get; set; }
        public string Code { get; set; }
        public int NumberAttending { get; set; }
        public int MaxInvited { get; set; }
    }
}
