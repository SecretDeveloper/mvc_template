﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Data;

namespace WeddingMVC.DAL
{
    public class GenericRepository<TEntity> :Interfaces.IRepository<TEntity> where TEntity : class
    {
        internal WeddingContext _context;
        internal DbSet<TEntity> _dbSet;

        public GenericRepository() 
        {}

        /*public GenericRepository(WeddingContext context)
        {
            this.SetContext(context);
        }
         */

        public void SetContext(WeddingContext context)
        {
            this._context = context;
            this._dbSet = context.Set<TEntity>();
        }

        public WeddingContext GetContext()
        {
            return _context;
        }

        public virtual IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return _dbSet;
        }

        public virtual IQueryable<TEntity> AsQueryable()
        {
            return _dbSet;
        }

        public virtual TEntity GetByID(object id)
        {
            return _dbSet.Find(id);
        }

        public virtual void Add(TEntity entity)
        {
            _dbSet.Add(entity);
        }

        public virtual void Attach(TEntity entity)
        {
            _dbSet.Attach(entity);
        }

        public virtual TEntity First(Expression<Func<TEntity, bool>> where)
        {
            return _dbSet.First<TEntity>(where);
        }

        public virtual TEntity Single(Expression<Func<TEntity, bool>> where)
        {
            return _dbSet.Single<TEntity>(where);
        }

        public virtual IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> where)
        {
            return _dbSet.Where<TEntity>(where);
        }

        public virtual void Delete(object id)
        {
            TEntity entityToDelete = _dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (_context.Entry(entityToDelete).State == EntityState.Detached)
            {
                _dbSet.Attach(entityToDelete);
            }
            _dbSet.Remove(entityToDelete);
        }

        public virtual void Update(TEntity entityToUpdate)
        {
            _dbSet.Attach(entityToUpdate);
            _context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public virtual void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void Dispose() 
           {
             Dispose(true);
              GC.SuppressFinalize(this); 
           }

        protected virtual void Dispose(bool disposing) 
           {
              if (disposing) 
              {
                 // Free other state (managed objects).
              }
              // Free your own state (unmanaged objects).
              // Set large fields to null.
           }

           // Use C# destructor syntax for finalization code.
        ~GenericRepository()
           {
              // Simply call Dispose(false).
              Dispose (false);
           }
    }
}
