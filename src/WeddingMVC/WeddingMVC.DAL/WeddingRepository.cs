﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace WeddingMVC.DAL
{
    /*
    public class WeddingRepository:Interfaces.IRepository 
    {
        public WeddingContext Context { get; set; }

        public WeddingRepository()
        {
            this.Context = new WeddingContext();
        }

        public WeddingRepository(WeddingContext context)
        {
            this.Context = context;
        }

        public IEnumerable<Models.Invitee> GetAllInvitees()
        {
            return this.Context.Invitees;
        }

        public IEnumerable<Models.Invitee> FindInvitee(System.Linq.Expressions.Expression<Func<Models.Invitee, bool>> where)
        {
            return this.Context.Invitees.Where<Models.Invitee>(where);
        }

        public Models.Invitee Single(System.Linq.Expressions.Expression<Func<Models.Invitee, bool>> where)
        {
            return this.Context.Invitees.Single<Models.Invitee>(where);
        }

        public Models.Invitee First(System.Linq.Expressions.Expression<Func<Models.Invitee, bool>> where)
        {
            return this.Context.Invitees.First<Models.Invitee>(where);
        }

        public void DeleteInvitee(Models.Invitee invitee)
        {
            this.Context.Invitees.Remove(invitee);
        }

        public void AddInvitee(Models.Invitee invitee)
        {
            this.Context.Invitees.Add(invitee);
        }

        public void AttachInvitee(Models.Invitee invitee)
        {
            this.Context.Invitees.Attach(invitee);
        }
    }
     */
}
