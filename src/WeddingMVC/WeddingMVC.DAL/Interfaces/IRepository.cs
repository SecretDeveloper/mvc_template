﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Linq.Expressions;


namespace WeddingMVC.DAL.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    /*public interface IRepository
    {
        //Invitees.
        IEnumerable<Models.Invitee> GetAllInvitees();
        IEnumerable<Models.Invitee> FindInvitee(Expression<Func<Models.Invitee, bool>> where);
        Models.Invitee Single(Expression<Func<Models.Invitee, bool>> where);
        Models.Invitee First(Expression<Func<Models.Invitee, bool>> where);
        void DeleteInvitee(Models.Invitee invitee);
        void AddInvitee(Models.Invitee invitee);
        void AttachInvitee(Models.Invitee invitee);
    }*/

    public interface ITest<T>
    {
        string GetName<T>(T val);
    }

    public class TestClass<T> : ITest<T>
    {
        public string GetName<T>(T val)
        {
            return val.ToString();
        }
    }

    public interface IRepository<T>:IDisposable where T : class
    {
        IQueryable<T> AsQueryable();

        IEnumerable<T> GetAll();
        IEnumerable<T> Find(Expression<Func<T, bool>> where);
        T Single(Expression<Func<T, bool>> where);
        T First(Expression<Func<T, bool>> where);

        void Delete(T entity);
        void Add(T entity);
        void Attach(T entity);
        void SaveChanges();
        void SetContext(WeddingContext context);
        WeddingContext GetContext();

    }
}
